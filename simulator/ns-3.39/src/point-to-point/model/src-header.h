#ifndef SRC_HEADER_H
#define SRC_HEADER_H

#include "ns3/buffer.h"
#include "ns3/header.h"

namespace ns3
{

/**
 * \ingroup Header
 * \brief Header for the SRC
 */

class SrcHeader : public Header
{
  public:
    SrcHeader(uint32_t flow_cnt,
              uint64_t link_rate,
              uint64_t left,
              uint64_t seq,
              uint32_t udp_dip,
              uint16_t udp_sport,
              uint16_t udp_pg);
    SrcHeader();
    virtual ~SrcHeader();

    void SetFlowCnt(uint32_t t);
    uint32_t GetFlowCnt() const;
    void SetLinkRate(uint64_t t);
    uint64_t GetLinkRate() const;
    void SetLeft(uint64_t t);
    uint64_t GetLeft() const;
    void SetSeq(uint64_t t);
    uint64_t GetSeq() const;
    void SetUDPDip(uint32_t t);
    uint32_t GetUDPDip() const;
    void SetUDPSport(uint16_t t);
    uint16_t GetUDPSport() const;
    void SetUDPPG(uint16_t t);
    uint16_t GetUDPPG() const;

    static TypeId GetTypeId();
    virtual TypeId GetInstanceTypeId() const;
    virtual void Print(std::ostream& os) const;
    virtual uint32_t GetSerializedSize() const;
    virtual void Serialize(Buffer::Iterator start) const;
    virtual uint32_t Deserialize(Buffer::Iterator start);

  private:
    uint32_t m_flow_cnt;
    uint64_t m_link_rate;
    uint64_t m_left;
    uint64_t m_seq;
    uint32_t m_udp_dip;
    uint16_t m_udp_sport;
    uint16_t m_udp_pg;
};

}; // namespace ns3

#endif /* SRC_HEADER */