#include "src-header.h"

#include "ns3/buffer.h"
#include "ns3/log.h"

#include <iostream>

NS_LOG_COMPONENT_DEFINE("SrcHeader");

namespace ns3
{

NS_OBJECT_ENSURE_REGISTERED(SrcHeader);

SrcHeader::SrcHeader(uint32_t flow_cnt,
                     uint64_t link_rate,
                     uint64_t left,
                     uint64_t seq,
                     uint32_t udp_dip,
                     uint16_t udp_sport,
                     uint16_t udp_pg)
    : m_flow_cnt(flow_cnt),
      m_link_rate(link_rate),
      m_left(left),
      m_seq(seq),
      m_udp_dip(udp_dip),
      m_udp_sport(udp_sport),
      m_udp_pg(udp_pg)
{
}

SrcHeader::SrcHeader()
    : m_flow_cnt(0),
      m_link_rate(0),
      m_left(0),
      m_seq(0),
      m_udp_dip(0),
      m_udp_sport(0),
      m_udp_pg(0)
{
}

SrcHeader::~SrcHeader()
{
}

void
SrcHeader::SetFlowCnt(uint32_t t)
{
    m_flow_cnt = t;
}

uint32_t
SrcHeader::GetFlowCnt() const
{
    return m_flow_cnt;
}

void
SrcHeader::SetLinkRate(uint64_t t)
{
    m_link_rate = t;
}

uint64_t
SrcHeader::GetLinkRate() const
{
    return m_link_rate;
}

void
SrcHeader::SetLeft(uint64_t t)
{
    m_left = t;
}

uint64_t
SrcHeader::GetLeft() const
{
    return m_left;
}

void
SrcHeader::SetSeq(uint64_t t)
{
    m_seq = t;
}

uint64_t
SrcHeader::GetSeq() const
{
    return m_seq;
}

void
SrcHeader::SetUDPDip(uint32_t t)
{
    m_udp_dip = t;
}

uint32_t
SrcHeader::GetUDPDip() const
{
    return m_udp_dip;
}

void
SrcHeader::SetUDPSport(uint16_t t)
{
    m_udp_sport = t;
}

uint16_t
SrcHeader::GetUDPSport() const
{
    return m_udp_sport;
}

void
SrcHeader::SetUDPPG(uint16_t t)
{
    m_udp_pg = t;
}

uint16_t
SrcHeader::GetUDPPG() const
{
    return m_udp_pg;
}

TypeId
SrcHeader::GetTypeId()
{
    static TypeId tid = TypeId("ns3::SrcHeader").SetParent<Header>().AddConstructor<SrcHeader>();
    return tid;
}

TypeId
SrcHeader::GetInstanceTypeId() const
{
    return GetTypeId();
}

void
SrcHeader::Print(std::ostream& os) const
{
    os << " flow_cnt: " << m_flow_cnt << " link_rate: " << m_link_rate;
}

uint32_t
SrcHeader::GetSerializedSize() const
{
    return sizeof(m_flow_cnt) + sizeof(m_link_rate) + sizeof(m_left) + sizeof(m_seq) +
           sizeof(m_udp_dip) + sizeof(m_udp_sport) + sizeof(m_udp_pg);
}

void
SrcHeader::Serialize(Buffer::Iterator start) const
{
    start.WriteU32(m_flow_cnt);
    start.WriteU64(m_link_rate);
    start.WriteU64(m_left);
    start.WriteU64(m_seq);
    start.WriteU32(m_udp_dip);
    start.WriteU16(m_udp_sport);
    start.WriteU16(m_udp_pg);
}

uint32_t
SrcHeader::Deserialize(Buffer::Iterator start)
{
    m_flow_cnt = start.ReadU32();
    m_link_rate = start.ReadU64();
    m_left = start.ReadU64();
    m_seq = start.ReadU64();
    m_udp_dip = start.ReadU32();
    m_udp_sport = start.ReadU16();
    m_udp_pg = start.ReadU16();

    return GetSerializedSize();
}

}; // namespace ns3
