import os

# algs = ['hpcc']
algs = ['hpcc', 'timely']
# algs = ['00', '01', '10', '11']
# algs = ['00', '11']
# algs = ['0', '1']
loads = ['0.2', '0.4', '0.6', '0.8', '0.9']
FLOW_MODE = ['0', '1', '2']
REQ_RATE=list(["1","4", "8", "16"])
REQ_SIZE=list(["2000000", "4000000", "6000000", "8000000"])

load="0.8"
req="0"
query="0"

for alg in algs:
	for workload in ['hadoop', 'google_rpc', 'websearch']:
		for load in loads:
			for pre in ['before', 'after']:
				def print_log(name):
					fct_slowdown = []
					fpname = '_'.join([pre, workload])
					path = os.path.join(fpname, name)
					with open(path, 'r') as f:
						for line in f:
							x = line.strip().split()
							assert x[0] == "FCT"
							if int(query) // 10 == int(x[3]): # abandon incast flow
								continue
							fct, base_fct = int(x[1]), int(x[5])
							fct_slowdown.append(fct / base_fct)
					fct_slowdown.sort()
					if len(fct_slowdown) != 0:
						print(path)
						print(f'Alg: {alg}  P95 FCT Slowdown: {fct_slowdown[int(len(fct_slowdown)) * 95 // 100]}')
						print(f'Alg: {alg}  P99 FCT Slowdown: {fct_slowdown[int(len(fct_slowdown)) * 99 // 100]}')
						print(f'Alg: {alg}  P999 FCT Slowdown: {fct_slowdown[int(len(fct_slowdown)) * 999 // 1000]}')
						print(f'Alg: {alg}  Pmax FCT Slowdown: {fct_slowdown[int(len(fct_slowdown) - 1)]}')
				if pre == 'before':
					name = 'result-'+alg+'-'+load+'-'+req+'-'+query+'.fct'
					print_log(name)
				else:
					for flow_mode in FLOW_MODE:
						name = 'result-'+alg+'-'+load+'-'+req+'-'+query+'-'+flow_mode+'.fct'
						print_log(name)
