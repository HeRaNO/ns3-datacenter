#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 06:46:30 2021

@author: vamsi
"""
import os
import requests
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import pylab
from matplotlib.lines import Line2D

########## WORKLOADS ##################

K=1000
M=K*K

algnames={}
algnames["before"]="w/o ToR++"
algnames["after_0"]="w/ ToR++ (SF)"
algnames["after_1"]="w/ ToR++ (Avg)"
algnames["after_2"]="w/ ToR++ (LF)"

markers={}
markers["before"]="-"
markers["after_0"]="--"
markers["after_1"]=":"
markers["after_2"]="-."

colors={}
colors["before"]='black'
colors["after_0"]='red'
colors["after_1"]='green'
colors["after_2"]='blue'

flowStep = [ 0,5*K, 10*K, 20*K, 30*K, 50*K, 75*K, 100*K, 200*K, 400*K,600*K,800*K, 1*M, 5*M, 10*M  ]
flowSteps= [ 5*K, 10*K, 20*K, 30*K, 50*K, 75*K, 100*K, 200*K, 400*K,600*K,800*K, 1*M, 5*M, 10*M ]
fS=np.arange(len(flowSteps))
flowSteps= [ "5K", "", "20K","", "50K", "", "100K","", "400K","","800K","", "5M", "10M" ]
loads=list(["0.2","0.4","0.6","0.8","0.9","0.95"])
REQ_RATE=list(["1","4", "8", "16"])
REQ_SIZE=list(["1000000", "2000000", "4000000", "6000000", "8000000", "10000000"])

plt.rcParams.update({'font.size': 18})
plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'font.serif': 'DejaVu Serif'})

loads=list(["0.2","0.4","0.6","0.8","0.9"])
loadInt=list([20,40,60,80,90])

with open("analyze/analyze_x_0_0", "r") as f:
    lines = f.readlines()

def plot_one_image(alg, workload):
    fig,(ax1,ax2) = plt.subplots(1,2,figsize=(10,5))
    ax1.xaxis.grid(True,ls='--')
    ax1.yaxis.grid(True,ls='--')
    ax2.xaxis.grid(True,ls='--')
    ax2.yaxis.grid(True,ls='--')

    bf_p95 = list()
    af_p95_0 = list()
    af_p95_1 = list()
    af_p95_2 = list()
    bf_p99 = list()
    af_p99_0 = list()
    af_p99_1 = list()
    af_p99_2 = list()

    for i in range(0, len(lines), 5):
        bfaf_wkld, filename = lines[i].split("/")
        bfaf, wkld = bfaf_wkld.split("_", 1)
        if wkld != workload or alg not in filename:
            continue
        p95 = float(lines[i + 1].split()[-1])
        p99 = float(lines[i + 2].split()[-1])
        if bfaf == "before":
            bf_p95.append(p95)
            bf_p99.append(p99)
        else:
            if "0.fct" in filename:
                af_p95_0.append(p95)
                af_p99_0.append(p99)
            elif "1.fct" in filename:
                af_p95_1.append(p95)
                af_p99_1.append(p99)
            else:
                af_p95_2.append(p95)
                af_p99_2.append(p99)

    ax1.plot(loadInt,bf_p95,label=algnames["before"],linestyle=markers["before"],lw=2,c=colors["before"])
    ax1.plot(loadInt,af_p95_0,label=algnames["after_0"],linestyle=markers["after_0"],lw=2,c=colors["after_0"])
    ax1.plot(loadInt,af_p95_1,label=algnames["after_1"],linestyle=markers["after_1"],lw=2,c=colors["after_1"])
    ax1.plot(loadInt,af_p95_2,label=algnames["after_2"],linestyle=markers["after_2"],lw=2,c=colors["after_2"])

    ax2.plot(loadInt,bf_p99,label=algnames["before"],linestyle=markers["before"],lw=2,c=colors["before"])
    ax2.plot(loadInt,af_p99_0,label=algnames["after_0"],linestyle=markers["after_0"],lw=2,c=colors["after_0"])
    ax2.plot(loadInt,af_p99_1,label=algnames["after_1"],linestyle=markers["after_1"],lw=2,c=colors["after_1"])
    ax2.plot(loadInt,af_p99_2,label=algnames["after_2"],linestyle=markers["after_2"],lw=2,c=colors["after_2"])

    ax1.set_xlabel('load (%)')
    ax1.set_ylabel('P95 FCT slowdown')
    ax2.set_xlabel('load (%)')
    ax2.set_ylabel('P99 FCT slowdown')

    fig.tight_layout()
    fig.savefig(f"images/load-fct-{alg}-{workload}.eps")
    fig.savefig(f"images/load-fct-{alg}-{workload}.png")

for alg in ['hpcc', 'timely']:
    for workload in ['hadoop', 'google_rpc', 'websearch']:
        plot_one_image(alg, workload)
