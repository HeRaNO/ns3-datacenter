#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 06:46:30 2021

@author: vamsi
"""
import os
import requests
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import pylab
from matplotlib.lines import Line2D

########## WORKLOADS ##################

workload="hadoop"
req="0"
query="0"
load="0.6"

def print_one_log(flow_mode):
    alg = "hpcc"
    for pre in ["before", "after"]:
        bfaf = "_".join([pre, workload])
        if pre == "before":
            name = bfaf+'/result-'+alg+'-'+load+'-'+req+'-'+query+'.buf'
        else:
            name = bfaf+'/result-'+alg+'-'+load+'-'+req+'-'+query+'-'+flow_mode+'.buf'
        df = pd.read_csv(name,delimiter=' ',usecols=[3],names=["qlen"])
        
        qlen=df["qlen"].tolist()
        sortQlen= np.sort(qlen)
        print(f"{bfaf} {alg} {flow_mode} max qlen: {sortQlen[-1]}")

    alg = "timely"
    for pre in ["before", "after"]:
        bfaf = "_".join([pre, workload])
        if pre == "before":
            name = bfaf+'/result-'+alg+'-'+load+'-'+req+'-'+query+'.buf'
        else:
            name = bfaf+'/result-'+alg+'-'+load+'-'+req+'-'+query+'-'+flow_mode+'.buf'
        df = pd.read_csv(name,delimiter=' ',usecols=[3],names=["qlen"])

        qlen=df["qlen"].tolist()
        sortQlen= np.sort(qlen)
        print(f"{bfaf} {alg} {flow_mode} max qlen: {sortQlen[-1]}")

for flow_mode in ['0', '1', '2']:
    print_one_log(flow_mode)
