#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 06:46:30 2021

@author: vamsi
"""
import os
import requests
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import pylab
from matplotlib.lines import Line2D

######### BURST ###############

plt.rcParams.update({'font.size': 18})
plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'font.serif': 'DejaVu Serif'})

algs=['hpcc', 'timely']

def plot_one_image(folder, name):
    df = pd.read_csv(folder+'/result-'+name+'.burst',delimiter=' ',usecols=[5,9,11,13],names=["th","qlen","time","power"])

    fig,ax = plt.subplots(1,1)
    ax.xaxis.grid(True,ls='--')
    ax.yaxis.grid(True,ls='--')
    ax1=ax.twinx()
    ax.set_yticks([10e9,25e9,40e9,80e9,100e9])
    ax.set_yticklabels(["10","25","40","80","100"])
    ax.set_ylabel("Throughput (Gbps)")
    
    start=0.15
    xtics=[i*0.001+start for i in range(0,6)]
    ax.set_xticks(xtics)
    xticklabels=[str(i) for i in range(0,6)]
    ax.set_xticklabels(xticklabels)
    
    ax.set_xlabel("Time (ms)")
    ax.set_xlim(0.1495,0.154)
    ax.plot(df["time"],df["th"],label="Throughput",c='blue',lw=2)
    ax1.set_ylim(0,600)
    ax1.set_ylabel("Queue length (KB)")
    ax1.plot(df["time"],df["qlen"]/(1000),c='orange',ls='--',label="Qlen",lw=2)
    fig.tight_layout()    
    fig.savefig('images/burst/'+name+'.eps')
    fig.savefig('images/burst/'+name+'.png')

for bfaf in ['before', 'after']:
    folder = "_".join(['results_burst', bfaf])
    if bfaf == "before":
        for alg in algs:
            plot_one_image(folder, alg)
    else:
        for alg in algs:
            for flow_mode in ['0', '1', '2']:
                plot_one_image(folder, "-".join([alg, flow_mode]))

figlegend = pylab.figure(figsize=(7,1))
lenged_elements = [Line2D([0],[0], color='blue',lw=2,label="Throughput"), Line2D([0],[0], color='orange',linestyle='--',lw=2,label="Queue length")]

figlegend.tight_layout()
figlegend.legend(handles=lenged_elements,loc=10,ncol=2, framealpha=0,fontsize=20)
figlegend.savefig('images/burst/burst-legend.png')
figlegend.savefig('images/burst/burst-legend.eps')
