for alg in ["hpcc", "timely"]:
    for bfaf in ["before", "after"]:
        def printLog(name):
            with open(name, "r") as f:
                fct_slowdown = []
                for line in f:
                    x = line.strip().split()
                    if True:
                    # if int(x[3]) <= 10000:
                    # if int(x[3]) > 10 ** 6 and int(x[3]) <= 10 ** 7:
                        fct, base_fct = int(x[1]), int(x[5])
                        fct_slowdown.append(fct / base_fct)
                fct_slowdown.sort()
                # for i in range(len(fct_slowdown)):
                #     if fct_slowdown[i] > 10000:
                #         break
                # print((i - 1) / len(fct_slowdown) * 100)
                p95 = fct_slowdown[len(fct_slowdown) * 95 // 100]
                p99 = fct_slowdown[len(fct_slowdown) * 99 // 100]
                print(f"{name} P95: {p95}")
                print(f"{name} P99: {p99}")
        if bfaf == "before":
            name = f"{bfaf}_hadoop/result-{alg}-0.6-0-0.fct"
            printLog(name)
        else:
            for flow_mode in ["0", "1", "2"]:
                name = f"{bfaf}_hadoop/result-{alg}-0.6-0-0-{flow_mode}.fct"
                printLog(name)
        
