# PowerTCP Simulations

To avoid any directory conflicts, first open `config.sh` and set the value of `NS3` to the FULL path to the current directory. Both Incast and Fairness simulations take only about a few minutes. So you can play around easily.

## Incast

- **Simulations:** Run `./script-burst.sh yes no` in your terminal to launch one-shot simulations for PowerTCP, Theta-PowerTCP, HPCC, TIMELY and DCQCN in a 10:1 incast scenario. The simulation data is written to `dump_burst/` folder in this directory. 
- **Parse results:** Just run `./results-burst.sh` and the results are written to `results_burst/` folder. 

## Workload

- **Simulations:** Run `./script-workload.sh` in your terminal to launch workload tests for HPCC and TIMELY. The simulation data is written to `dump_workload/`. The script launches simulations for the following cases:
	- varying loads; no incast traffic
	- 80% load, 2MB request size and varying request rates
	- 80% load, 1/second request rate and varying request sizes
- **Parse results:** Run `./results-workload.sh` and the results can be found in `results_workload/`. 

**IMPORTANT to note:** These simulations take a very long time. The scripts are written to launch **30** simulations in parallel. Please adjust this number based on your cpu and memory (one experiment on Google_AllRPC workload needs ~3GB memory). Below are the lines to change (they appear multiple times in the script. Pay attention!).

```bash
while [[ $(ps aux|grep "powertcp-evaluation-workload-optimized"|wc -l) -gt 30 ]];do
	echo "Waiting for cpu cores.... $N-th experiment "
	sleep 60
done
```