#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 06:46:30 2021

@author: vamsi
"""
import os
import requests
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import pylab
from matplotlib.lines import Line2D

########## WORKLOADS ##################

K=1000
M=K*K

algnames={}
algnames["before"]="w/o ToR++"
algnames["after_0"]="w/ ToR++ (SF)"
algnames["after_1"]="w/ ToR++ (Avg)"
algnames["after_2"]="w/ ToR++ (LF)"

markers={}
markers["before"]="x"
markers["after_0"]="s"
markers["after_1"]="*"
markers["after_2"]="."

linestyles={}
linestyles["before"]="-"
linestyles["after_0"]="--"
linestyles["after_1"]=":"
linestyles["after_2"]="-."

colors={}
colors["before"]='black'
colors["after_0"]='red'
colors["after_1"]='green'
colors["after_2"]='blue'

flowStep = [ 0,5*K, 10*K, 20*K, 30*K, 50*K, 75*K, 100*K, 200*K, 400*K,600*K,800*K, 1*M, 5*M, 10*M  ]
flowSteps= [ 5*K, 10*K, 20*K, 30*K, 50*K, 75*K, 100*K, 200*K, 400*K,600*K,800*K, 1*M, 5*M, 10*M ]
fS=np.arange(len(flowSteps))
flowSteps= [ "5K", "", "20K","", "50K", "", "100K","", "400K","","800K","", "5M", "10M" ]
loads=list(["0.2","0.4","0.6","0.8","0.9","0.95"])
REQ_RATE=list(["1","4", "8", "16"])
REQ_SIZE=list(["2", "4", "6", "8", "10"])

plt.rcParams.update({'font.size': 18})
plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'font.serif': 'DejaVu Serif'})

def plot_one_image(workload):
    req="0"
    query="0"
    load="0.8"

    fig,(ax1,ax2) = plt.subplots(1,2,figsize=(10,5))

    ax1.set_xticks([0,100*K,200*K,300*K,400*K,500*K])
    ax1.set_xticklabels(["0","100","200","300","400","500"])
    ax1.set_ylabel("HPCC CDF")
    ax1.set_xlabel("Buffer occupancy (KB)")

    ax2.set_xticks([0,200*K,400*K,600*K,800*K,1*M])
    ax2.set_xticklabels(["0","200","400","600","800","1000"])
    ax2.set_ylabel("TIMELY CDF")
    ax2.set_xlabel("Buffer occupancy (KB)")

    # query="2000000"
    # req="16"
    # load="0.8"

    # fig,(ax1,ax2) = plt.subplots(1,2,figsize=(10,5))

    # ax1.set_xticks([0,0.5*M,1*M,1.5*M,2*M,2.5*M])
    # ax1.set_xticklabels(["0","0.5","1","1.5","2","2.5"])
    # ax1.set_ylabel("HPCC CDF")
    # ax1.set_xlabel("Buffer occupancy (MB)")

    # ax2.set_xticks([0,0.5*M,1*M,1.5*M,2*M,2.5*M])
    # ax2.set_xticklabels(["0","0.5","1","1.5","2","2.5"])
    # ax2.set_ylabel("TIMELY CDF")
    # ax2.set_xlabel("Buffer occupancy (MB)")

    def get_data(name):
        df = pd.read_csv(name,delimiter=' ',usecols=[3],names=["qlen"])
        
        qlen=df["qlen"].tolist()
        sortQlen= np.sort(qlen)
        p = 1. * np.arange(len(sortQlen))/(len(sortQlen) - 1)

        return sortQlen, p

    alg = "hpcc"
    for pre in ["before", "after"]:
        folder_name="_".join([pre,workload])
        if pre == "before":
            bfaf = pre
            name = folder_name+'/result-'+alg+'-'+load+'-'+req+'-'+query+'.buf'
            sortQlen, p = get_data(name)
            ax1.plot(sortQlen,p,linestyle=linestyles[bfaf],c=colors[bfaf],lw=2)
            ax1.plot(sortQlen[len(sortQlen)-1],1,marker=markers[bfaf],markersize=10,c=colors[bfaf],label=algnames[bfaf])
        else:
            for flow_mode in ['0', '1', '2']:
                bfaf = "_".join([pre, flow_mode])
                name = folder_name+'/result-'+alg+'-'+load+'-'+req+'-'+query+'-'+flow_mode+'.buf'
                sortQlen, p = get_data(name)
                ax1.plot(sortQlen,p,linestyle=linestyles[bfaf],c=colors[bfaf],lw=2)
                ax1.plot(sortQlen[len(sortQlen)-1],1,marker=markers[bfaf],markersize=10,c=colors[bfaf],label=algnames[bfaf])

    alg = "timely"
    for pre in ["before", "after"]:
        folder_name="_".join([pre,workload])
        if pre == "before":
            bfaf = pre
            name = folder_name+'/result-'+alg+'-'+load+'-'+req+'-'+query+'.buf'
            sortQlen, p = get_data(name)
            ax2.plot(sortQlen,p,linestyle=linestyles[bfaf],c=colors[bfaf],lw=2)
            ax2.plot(sortQlen[len(sortQlen)-1],1,marker=markers[bfaf],markersize=10,c=colors[bfaf],label=algnames[bfaf])
        else:
            for flow_mode in ['0', '1', '2']:
                bfaf = "_".join([pre, flow_mode])
                name = folder_name+'/result-'+alg+'-'+load+'-'+req+'-'+query+'-'+flow_mode+'.buf'
                sortQlen, p = get_data(name)
                ax2.plot(sortQlen,p,linestyle=linestyles[bfaf],c=colors[bfaf],lw=2)
                ax2.plot(sortQlen[len(sortQlen)-1],1,marker=markers[bfaf],markersize=10,c=colors[bfaf],label=algnames[bfaf])


    fig.tight_layout()
    fig.savefig(f'images/buf-load-{workload}-{load}-{req}-{query}.eps')
    fig.savefig(f'images/buf-load-{workload}-{load}-{req}-{query}.png')

for workload in ['hadoop', 'google_rpc', 'websearch']:
    plot_one_image(workload)
