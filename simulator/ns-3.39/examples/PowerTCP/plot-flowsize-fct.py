#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 06:46:30 2021

@author: vamsi
"""
import os
import requests
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import pylab
from matplotlib.lines import Line2D

########## WORKLOADS ##################

K=1000
M=K*K

algs=list(["before", "after_0", "after_1", "after_2"])

algnames={}
algnames["before"]="w/o ToR++"
algnames["after_0"]="w/ ToR++ (SF)"
algnames["after_1"]="w/ ToR++ (Avg)"
algnames["after_2"]="w/ ToR++ (LF)"

markers={}
markers["before"]="-"
markers["after_0"]="--"
markers["after_1"]=":"
markers["after_2"]="-."

colors={}
colors["before"]='black'
colors["after_0"]='red'
colors["after_1"]='green'
colors["after_2"]='blue'

flowStep = [ 0,5*K, 10*K, 20*K, 30*K, 50*K, 75*K, 100*K, 200*K, 400*K,600*K,800*K, 1*M, 5*M, 10*M  ]
flowSteps= [ 5*K, 10*K, 20*K, 30*K, 50*K, 75*K, 100*K, 200*K, 400*K,600*K,800*K, 1*M, 5*M, 10*M ]
fS=np.arange(len(flowSteps))
flowSteps= [ "5K", "", "20K","", "50K", "", "100K","", "400K","","800K","", "5M", "10M" ]
loads=list(["0.2","0.4","0.6","0.8","0.9","0.95"])
REQ_RATE=list(["1","4", "8", "16"])
REQ_SIZE=list(["1000000", "2000000", "4000000", "6000000", "8000000", "10000000"])

#######################

# FLOWSIZE VS FCT

#######################

req="0"
query="0"

plt.rcParams.update({'font.size': 18})
plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'font.serif': 'DejaVu Serif'})

load="0.6"
alg="hpcc"

fig,(ax1,ax2) = plt.subplots(1,2,figsize=(10,5))
ax1.set_label("Flow size")
ax1.set_ylabel("P95 FCT slowdown")
ax1.set_xlabel("Flow size (bytes)")
ax1.set_yscale('linear')
ax1.set_ylim(1,3.5)
ax1.set_xticks(fS)
ax1.set_xticklabels(flowSteps,rotation=45)
ax1.set_yticks([i for i in range(1,4)])
ax1.set_yticklabels([i for i in range(1,4)])

ax2.set_label("Flow size")
ax2.set_ylabel("P99 FCT slowdown")
ax2.set_xlabel("Flow size (bytes)")
ax2.set_yscale('log')
ax2.set_ylim(2.5,6)
ax2.set_xticks(fS)
ax2.set_xticklabels(flowSteps,rotation=45)
ax2.set_yticks([i for i in range(3,7)])
ax2.set_yticklabels([i for i in range(3,7)])

# alg="timely"

# fig,(ax1,ax2) = plt.subplots(1,2,figsize=(10,5))
# ax1.set_label("Flow size")
# ax1.set_ylabel("P95 FCT slowdown")
# ax1.set_xlabel("Flow size (bytes)")
# ax1.set_yscale('log')
# ax1.set_ylim(2,60) # 60 for flow_mode=2
# ax1.set_xticks(fS)
# ax1.set_xticklabels(flowSteps,rotation=45)
# ax1.set_yticks([i for i in range(10,60,10)])
# ax1.set_yticklabels([i for i in range(10,60,10)])

# ax2.set_label("Flow size")
# ax2.set_ylabel("P99 FCT slowdown")
# ax2.set_xlabel("Flow size (bytes)")
# ax2.set_yscale('log')
# ax2.set_ylim(3,75)
# ax2.set_xticks(fS)
# ax2.set_xticklabels(flowSteps,rotation=45)
# ax2.set_yticks([i for i in range(10,70,10)])
# ax2.set_yticklabels([i for i in range(10,70,10)])

df = pd.read_csv('before_hadoop/result-'+alg+'-'+load+'-0-0.fct',delimiter=' ',usecols=[1,3,5],names=["fct","size","base"])
before_lfct99=list()
before_lfct95=list()
for i in range(1,len(flowStep)):
    df1=df.loc[ (df["size"]<flowStep[i]) & (df["size"] >= flowStep[i-1]) ]
    fcts=df1["fct"].to_list()
    basefcts=df1["base"].to_list()
    sd=list([fcts[i]/basefcts[i] for i in range(len(fcts))])
    sd.sort()
    try:
        fct99 = sd[int(len(sd)*0.99)]
    except:
        fct99 = 0
    try:
        fct95 = sd[int(len(sd)*0.95)]
    except:
        fct95 = 0
    before_lfct99.append(fct99)
    before_lfct95.append(fct95)

ax1.plot(fS,before_lfct95,label=algnames['before'],linestyle=markers['before'],lw=2,c=colors['before'])
ax2.plot(fS,before_lfct99,label=algnames['before'],linestyle=markers['before'],lw=2,c=colors['before'])

for flow_mode in ['0', '1', '2']:
    df = pd.read_csv('after_hadoop/result-'+alg+'-'+load+'-0-0-'+flow_mode+'.fct',delimiter=' ',usecols=[1,3,5],names=["fct","size","base"])
    after_lfct99=list()
    after_lfct95=list()
    for i in range(1,len(flowStep)):
        df1=df.loc[ (df["size"]<flowStep[i]) & (df["size"] >= flowStep[i-1]) ]
        fcts=df1["fct"].to_list()
        basefcts=df1["base"].to_list()
        sd=list([fcts[i]/basefcts[i] for i in range(len(fcts))])
        sd.sort()
        try:
            fct99 = sd[int(len(sd)*0.99)]
        except:
            fct99 = 0
        try:
            fct95 = sd[int(len(sd)*0.95)]
        except:
            fct95 = 0
        after_lfct99.append(fct99)
        after_lfct95.append(fct95)

    ax1.plot(fS,after_lfct95,label=algnames[f'after_{flow_mode}'],linestyle=markers[f'after_{flow_mode}'],lw=2,c=colors[f'after_{flow_mode}'])
    ax2.plot(fS,after_lfct99,label=algnames[f'after_{flow_mode}'],linestyle=markers[f'after_{flow_mode}'],lw=2,c=colors[f'after_{flow_mode}'])

fig.tight_layout()
fig.savefig('images/fct-'+alg+'-'+load+'.eps')
fig.savefig('images/fct-'+alg+'-'+load+'.png')

figlegend = pylab.figure(figsize=(14,1))
lenged_elements=list()

for alg in algs:
    lenged_elements.append(Line2D([0],[0], color=colors[alg],linestyle=markers[alg],lw=3,label=algnames[alg]))

figlegend.tight_layout()
figlegend.legend(handles=lenged_elements,loc=10,ncols=4, framealpha=0,fontsize=20)
figlegend.savefig('images/all-legend.png')
figlegend.savefig('images/all-legend.eps')
